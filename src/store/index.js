import Vue from 'vue';
import Vuex from 'vuex';

import requests from '../utils/requests';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    photos: [],
    compareList: [],
    error: '',
  },
  getters: {
    isInCompareList: (state) => (id) => {
      console.log(state);

      return state.compareList.some((photo) => photo.id === id);
    },
  },
  actions: {
    getPhotos: async function ({ commit }) {
      try {
        const response = await requests.get('/photos');
        console.log(response);
        commit('APPEND_PHOTO', response.data);
      } catch (error) {
        console.log(error);
      }
    },

    addToCompare: function ({ commit }, id) {
      commit('ADD_TO_COMPARE', id);
    },
    removeFromCompare: function ({ commit }, id) {
      commit('REMOVE_FROM_COMPARE', id);
    },
  },
  mutations: {
    APPEND_PHOTO: (state, data) => {
      state.photos = data.slice(0, 20);
    },
    ADD_TO_COMPARE: (state, id) => {
      let photo = state.photos.filter((photos) => photos.id === id)[0];

      if (!state.compareList.some((list) => list.id === id)) {
        state.compareList = [...state.compareList, photo];
      }
    },
    REMOVE_FROM_COMPARE: (state, id) => {
      state.compareList = state.compareList.filter((photo) => photo.id !== id);
    },
  },
});
